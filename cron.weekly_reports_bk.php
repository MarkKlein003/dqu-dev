#!/usr/bin/php
<?php
// require_once('../dequeComponents/_init/dqu.init.php');
require_once('../dequeComponents/dqu.php');

// TODO: Append to an error array, and if that is non-empty when you are ready to exit, email the contents to admin.

$report_dir_nm = 'reports_WEEKLY';

$dqu = new dqu();
define( 'EOL', "\r\n" );
$err = array();
$out = '';

/*
//----------------------
// YOU CAN'T REBUILD THE COURSE TREE CACHE HERE BECAUSE MODX IS CALLED INTERNALLY TO OBTAIN TEMPLATE VARIABLE VALUES
// YOU CAN'T REBUILD THE COURSE TREE CACHE HERE BECAUSE MODX IS CALLED INTERNALLY TO OBTAIN TEMPLATE VARIABLE VALUES
// YOU CAN'T REBUILD THE COURSE TREE CACHE HERE BECAUSE MODX IS CALLED INTERNALLY TO OBTAIN TEMPLATE VARIABLE VALUES

// When you are certain that you have a correctly working routine to read those values, you can change the code that builds the raw
// course tree and then repopulate the course tree cache from here.
//----------------------
*/

//----------------------
///*
set_time_limit( 60 * 20 );

$s = microtime( true );

$result = $dqu->repop_viewable_pg_cache();

$e = microtime( true );

$d = $e - $s;

if( $result === null )
{
   $out .= 'repop_viewable_pg_cache() returned NULL!';
   return $out;
}

$out .= "<pre>Done with repop_viewable_pg_cache() in $d seconds. Records added: $result</pre>";

//++++++++++++++++++++++++
echo $out;
//return;
//++++++++++++++++++++++++
//*/
//----------------------

//  Run these client reports - New //
//$clientlist = array(1508,1737,2119);



$now = $dqu->mysql_now( true );
$today = $now['date'];

$start_time = microtime( true );

// foreach( $clientlist as $clid => $cl )
// {
    //$clients = $dqu->get_clients($clid);
    //$clients = $dqu->get_clients();
    $clients = $dqu->get_clients(2119);
    
    // ---- Client List ---- //
    // 1737 // Accenture
    // 1508 // CVS
    // 2119 // LinkedIn
    // 747  // Intuit - no weekly reports //
    // 1610 // DLA Defense Logistics Agency //
    // 38   // Wells Fargo //
    // 3843 // Knewton 
    // 817  // State Farm // 
    // 2047 // GVSU //
    
    // echo $clid . EO./L;

    foreach( $clients as $cid => $co )
    {

       $subs = $dqu->get_client_subs( $cid, false );

       foreach( $subs as $sid => $so )
       {
          //++++++++++++++++++++++++++++++++++++++++++++++
          // USE THIS LINE TO ONLY RUN CERTAIN REPORTS FOR THE CLIENT'S ABOVE
          //if( !in_array( $sid, array(1549)) ) continue;
          // if( !in_array( $sid, array(1738,1739,1746,1549,2120) ) ) continue;
          // if( !in_array( $sid, array(756,757) ) ) continue;
          if( !in_array( $sid, array(2120)) ) continue;
          // 1738, 1739, 1746, 2156     // Accenture Subscriptions
          // 1549                       // CVS Full Curriculum
          // 2120                       // LinkedIn 
          // 756, 757                   // Intuit - Don't need weekly reports!!! 
          // 1611                       // Defense Logistics Agency
          // 856, 2452                  // Wells Fargo - Don't need weekly reports!!!
          // 3844, 3845                 // Knewton
          // 1484, 3876                 // State Farm
          // 4105,2048                  // GVSU
          //++++++++++++++++++++++++++++++++++++++++++++++
    
          $dir_name = $report_dir_nm.'/client_'.$cid.'/subscr_'.$sid;
          $file_name = $dir_name.'/'.$today.'_c_'.$cid.'_s_'.$sid.'.csv';
    
          $res = @mkdir( $dir_name, 0777, true );
    
          //if( $res === false ) echo 'failed on '.$dir_name;
    
          $out = print_report( $dqu, $sid );
    
          $f = fopen( $file_name, 'xb' );         // create for writing; fail if it already exists
    
          if( $f === false )
          {
             echo( 'fopen() failed when writing reports' );
             return;
          }
    
          $w_ct = fwrite( $f, $out );
    
          if( $w_ct === false )
          {
             fclose( $f );
             echo( 'fwrite() failed when writing reports' );
             return;
          }
    
          fclose( $f );
       }
    } 
// } 

$end_time = microtime( true );
$total_time = (($end_time - $start_time)/60);

echo( 'total time: '.$total_time.EOL );

return;

function print_report( $dqu, $sid )
{
   $out = '';

   //$info = $dqu->get_subscription_info( $sid );

   //--------------
   $init_users = $dqu->get_subscription_users( $sid );
   if( $init_users === null ) return print_r( $dqu->last_err, true );

   $users = array();

   foreach( $init_users as $uid => $u )
   {
      //+++++++++++++++++++++++++++++
      // USERS TO IGNORE
      if( !$u->active ) continue;
      if( $u->username == 'andrew_moore@manulife.com' ) continue;    // he has a trial subscription that has expired
      if( $u->username == 'dqu_statefarm' ) continue;                // this is a demo user
      if( $u->username == 'wells-fargo-links' ) continue;
      if( $u->username == 'jonathanpowers22@gmail.com' ) continue;
    //  if( preg_match( '/^scorm_.*/', $u->username ) ) continue;
    //  if( preg_match( '/^wellsfargo.*/', $u->username ) ) continue;
      //if( preg_match( '/^jonathan\.powers.*/', $u->username ) ) continue;
      //if( preg_match( '/^paul\.bohman.*/', $u->username ) ) continue;
      //+++++++++++++++++++++++++++++

      $users[ $uid ] = $u;
   }

   unset( $init_users );

   $dqu->sort_users_by_full_name( $users );
   //--------------

   $quiz_pg_ids = $dqu->get_quiz_pg_ids();

   //--------------
   // get the RGs that the group has rights to see (at all access levels)
   $rgs = $dqu->get_viewable_rgs_from_ug( $sid, 0 );

   // FIXME: This won't get COURSES that are assigned to the parent (client) user group, but that shouldn't be a problem.
   // FIXME: This won't get COURSES that are assigned to the parent (client) user group, but that shouldn't be a problem.
   // FIXME: This won't get COURSES that are assigned to the parent (client) user group, but that shouldn't be a problem.

   $courses = $dqu->find_courses_from_rgs( $rgs );
   if( $courses === null ) return print_r( $dqu->last_err, true );
   //--------------

   //--------------
   $out .= '"name","username"';
   foreach( $courses as $c ) $out .= ',"'.$c->name.'"';
   $out .= "\n";
   foreach( $users as $uid => $u )
   {
      set_time_limit( 20 * 60 );

      $out .= '"'.$u->full_name.'","'.$u->username.'"';

      $pgs = $dqu->get_viewable_pgs_from_cache( $uid );

      foreach( $courses as $cid => $c )
      {
         $out .= ',';

         if( $pgs === null ) { $out .= 'ERR1'; continue; }
         if( empty( $pgs ) )
         {
            $dqu->db_log('user '.$uid.' has no view access to any page');
            $out .= 'n/a';
            continue;
         }
         if( $pgs[0] === 'error' ) { $out .= 'ERR2'; continue; }

         $c_tree = $dqu->get_credit_tree_from_cache( $c->root_pg );
         if( $c_tree === null ) { $out .= 'ERR3'; continue; }

         // TODO: Refer to the "test:" area in this file to get the algo. If percent is zero, output "-"
         //       If the user has no access, print "n/a".
         //       If there are any errors, just print "ERR".

         if( $pgs[0] === 'sudo' )
         {
            $credit_pgs = array_keys( $c_tree );
         }
         else
         {
            $dqu->filter_tree( $c_tree, $pgs );

            if( empty( $c_tree ) )
            {
               $out .= 'n/a';
               continue;
            }

            $credit_pgs = array_keys( $c_tree );
         }

         $credit_quiz_pgs = array_intersect( $credit_pgs, $quiz_pg_ids );
         $credit_info_pgs = array_diff( $credit_pgs, $credit_quiz_pgs );

         /*
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         This does not call the two procs below with a start date. This is OK as long as the user doesn't have an expired course attempt, which
         currently no non-SCORM user does, but this needs to be fixed. You would get the start date of the user's last non-expired attempt and use
         that for these calls. Do you have a proc for that? You must have such a thing in the global script. I would look there before looking in
         the other report-generating code, because the one(s) I just checked don't bother with a start date either.

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         */

         $credit_info_pgs_visited = $dqu->get_visited_pgs( $uid, $credit_info_pgs, 0 );            // FIXME: SKIPPED START

         $credit_quiz_pgs_passed = $dqu->get_quizzes_by_state( $uid, $credit_quiz_pgs, 'pass' );   // FIXME: SKIPPED START

         if( !is_array($credit_info_pgs_visited) || !is_array( $credit_quiz_pgs_passed ) )
         {
            $out .= 'ERR4';
            continue;
         }

         //-----------------------
         $num = count( $credit_info_pgs_visited ) + count( $credit_quiz_pgs_passed );
         $denom = count($credit_info_pgs) + count($credit_quiz_pgs);
         if( $denom == 0 ) { $out .= 'ERR5'; continue; }
         $percent = floor( ($num * 100) / $denom );

         $out .= '"';
         if( $percent == 0 )
         {
            $out .= '-';
         }
         else
         {
            $out .= $percent.'%';
            $out .= " ($num/$denom pgs)";

            /*
            // shows the last date the user visited the course (actually, only credit pages of the course)
            $last_date = '';
            $dqu->get_last_visited_pg( $uid, $credit_pgs, $last_date );
            $out .= "\n[".$last_date.']';
            */

            if( $percent == 100 )
            {
               $last_comp_date = $dqu->get_latest_non_expired_completion_date( $uid, $c->root_pg );

               if( $last_comp_date === false )
               {
                  $out .= "\n".$dqu->TEMPORARY__calc_course_end( $uid, $credit_pgs );
               }
               else
               {
                  $out .= "\n".$last_comp_date;
               }
            }
            else
            {
               $last_comp_date = $dqu->get_latest_non_expired_completion_date( $uid, $c->root_pg );

               if( $last_comp_date !== false )
               {
                  $out .= "\n".'100% on '.$last_comp_date;
               }
            }
         }
         $out .= '"';
         //-----------------------
      }
      $out .= "\n";
   }

   return $out;
}

//--------------

//----------------------
done:

if( !empty( $err ) )
{
   // TODO: send the admin an email with the errors in it
}
else
{
   // TODO: email the admin that this happened successfully
}

echo $out;
return;
//----------------------
?>
