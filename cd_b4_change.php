<?php
/**
* DESCRIPTION
*
* 1) Displays the client dashboard reports page.
* 2) Uses the deque.clientdashboard.class.php page.
* 
*
* PROPERTIES:
* Querys the deq_role_permissions table for permissions.
* 
* 
* 
*
* USAGE:
* Uses the user id of the person logged in to determine report rights 
* 
* 
* 
*
* TO DO:
* 
* 
*
* @author Mark Kleinknecht
*/

// Require autoloader //
require_once('../dequeComponents/_init/dqu.init.php');

// Create clientDashboard object using autoloader //
$clientDashboard = new ClientDashboard($modx, $dqu, $scriptProperties);

// Get the client dashboard page phrases //
$langFile = 'clientdashboard/clientdashboard.page.lang.php';
require_once( PATH_LANG . $langFile );
$pageText = $LANG['clientdashboard']['page'];

// Check user permissions //
$permissions = $clientDashboard->permissions($dqu->user->id);

if($permissions)
{
    echo '<p>' . $pageText['cvs'] . '</p>'."\n";
    
    // Make new session array to hold reports //
    $_SESSION['dqu']['clientReport'] = "";
    $_SESSION['dqu']['clientReport'] = array();
    
    $r = null;
    
    foreach ($permissions as $key => $p) 
    {
        
        $firstDateActive = false;
        $firstDatePrevious = false;
        $subCnt = 0;
        
        
        $subs = $clientDashboard->subscriptionSummary($p->role_id, $p->client_id, $p->subsciption_id, $p->scope);
        
        echo '<div class="deque-wrapper">'."\n";
        echo '  <h2 style="margin-top:-15px;">Course Progress Reports for ' . $p->client_name . '</h2>'."\n";
        echo '  <p>' . $pageText['designated'] . $p->role_name . ' for <strong>' . $p->client_name . '</strong>.</p>'."\n";   
        echo '  <div class="deque-tabpanel" data-deque-tabpanel-autoselect="true" data-deque-tabpanel-autoplay="false" id="report' . $r .'">'."\n";
        echo '      <ul role="tablist">'."\n";
        echo '          <li role="tab" aria-controls="panel-active">Active Subscriptions</li>'."\n";
        echo '          <li role="tab" aria-controls="panel-inactive">Inactive Subscriptions</li>'."\n";
        echo '      </ul>'."\n";
        echo '      <div class="deque-tabpanel-group">'."\n";
        echo '          <div role="tabpanel" id="panel-active">'."\n";
        
        // ---- Active Subscriptions - Loop through each subscription ---------------------------------------------------------  //
        foreach ($subs as $key1 => $s) 
        {
            
            if ($s->expired != "yes")
            {
                
                $list = null;
                $out = null;
                $firstreport = 0;
                $list = $dqu->get_subs_report_list( $p->client_id, $key1 );

                //if ($subCnt == 0)
                if (($list) && ($subCnt == 0))
                {
                    $out .= '<ul>'."\n";
                    
                }
                
                //  Only show if there are reports //
                if( empty( $list ) && ( $subs ))
                {
                    // $out .= '1 No reports available'."\n";
                } else
                if(( empty( $list ) && ( empty( $subs ))))
                {
                    $out .= '<p>No Active Subscriptions available.</p>'."\n";
                } else
                {
                    // ---- Active Subscriptions - Loop through each subscription report ------------------- //
                    foreach( $list as $i )
                    {
                        if ($firstreport < 1)
                        {
                            if($firstDateActive == 0)
                            {
                                echo '<p>Most recent report (' . $i . ')</p>'."\n";
                                $firstDateActive = true;
                            }
                            
                            $randomtoken = $dqu->generate_rand_key( 8 );
                         
                            // Create session array //;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->name= $s->name;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->cid = $p->client_id;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->sid = $key1;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->reportdate = $i;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->role = $p->role_id;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->scope = $p->scope;
                         
                            $out .= '   <li><a class="mono" href="dashboard2/subscriptionreport?id=' . $randomtoken .'" title= "Download File">'. $s->name .'</a></li>'."\n";
                            $firstreport++;
                        }
                    }
                    echo $out;
                    $out = null;
                }
                $subCnt++; 
            }
            
            
        }
        if($firstDateActive == true)
        {
        $out = '</ul>'."\n";
        echo $out;
        $out = null;
        }
        
        if ($firstDateActive == false)
        {
            echo '<p>No active subscriptions.</p>'."\n";
            $noShowPrevious = true;
        }
        // Expand/Collasp goes here //
        // ---- Previous Active Subscription Reports --------------------------------- //
        $totalListCnt =0;
        if( empty($subs) )
        {
            //  Dont show the previous reports //
            
        } else 
        {
            // Loop thorugh the reports count them //
            $totalListCnt =0;
            foreach ($subs as $key => $s) {
                if ($s->expired != "yes")
                {
                    $list = $dqu->get_subs_report_list( $p->client_id, $key ); 
                    
                    //  Count the reports and subtract 1 for the first report //
                    $listCnt = count($list);
                    $listCnt = $listCnt - 1;
                    $totalListCnt = $totalListCnt + $listCnt;
                }
            }

            echo '<br />'."\n";
            if( $totalListCnt < 1 )
            {
                //  Dont show the previous reports //
            } else 
            {
                
                $out = '            <div class="deque-expander" id="previous_reports">' ."\n";
                $out .= '               <div class="deque-expander-summary">Show Previous Active Subscription Reports</div>'."\n";
                $out .= '               <div class="deque-expander-content">'."\n";
                
                $subInactiveCnt = 0;
                
                // Previous Reports - Loop through each subscription //
                foreach ($subs as $key => $s) {
                    $subInactiveCnt = 0;
                    if ($s->expired != "yes")
                    {
                        $out .= '<h4>' . $s->name . '</h4>'."\n";
            
                        $list = null;
                        $list = $dqu->get_subs_report_list( $p->client_id, $key );
                        if (($subInactiveCnt == 0) && ($list))
                        {
                            $out .= '<ul>'."\n";
                        }
                        
                        if( empty( $list ) )
                        {
                            $out .= '<p>No reports available<p>'."\n";
                        } else
                        {
                            $previousReportCnt = 0;
                            
                            foreach( $list as $i )
                            {
                                //  Don't display the first report, it is the newest. //
                                if( $previousReportCnt > 0 )
                                {
                                    $randomtoken = $dqu->generate_rand_key( 8 );
                                 
                                    // Create session array //
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->name= $s->name;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->cid = $p->client_id;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->sid = $key;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->reportdate = $i;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->role = $p->role_id;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->scope = $p->scope;
                                 
                                    $out .= '   <li><a class="mono" href="dashboard2/subscriptionreport?id=' . $randomtoken .'" title= "Download File">'.$i.' CSV</a></li>'."\n";
                                    $subInactiveCnt++;
                                }
                                $previousReportCnt++;
                            }
                        }
                        if ($previousReportCnt > 1)
                        {
                            $out .= '</ul>'."\n";
                        }
                        if( $previousReportCnt == 1 )
                        {
                            $out .= '   <li>No reports available</li>' ."\n" . '</ul>'."\n";
                        }
                    }
                }
                
                $out .= '               </div>'."\n";
                $out .= '           </div>'."\n";
            
            }
            echo $out;
            $out = null;
        }

        echo        '       </div>'."\n";
        echo        '       <div role="tabpanel" id="panel-inactive">'."\n";
        
        $firstdate = 0;
        $subInactiveCnt = 0;
        
        // ---- Inactive Subscriptions - Loop through each subscription --------------------------------  //
        foreach ($subs as $key1 => $s) 
        {
            if ($s->expired == "yes")
            {
                $firstreport = 0;
                $list = null;
                $out = null;
                
                $list = $dqu->get_subs_report_list( $p->client_id, $key1 );

                if ($subInactiveCnt == 0)
                {
                    $out .= '<ul>'."\n";
                }
                
                if( empty( $list ) )
                {
                    $out .= '   <li>No reports available</li>' ."\n" . '</ul>'."\n";
                } else
                {
                    foreach( $list as $i )
                    {
                        //  Dont show the first report //
                        if ($firstreport < 1)
                        {
                            if($firstDatePrevious == false)
                            {
                                echo '<p>Most recent report (' . $i . ')</p>' . "\n";
                                $firstDatePrevious = true;
                            }
                            
                            $randomtoken = $dqu->generate_rand_key( 8 );
                         
                            // Create session array //
                            $_SESSION['dqu']['clientReport'][$randomtoken]->name= $s->name;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->cid = $p->client_id;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->sid = $key1;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->reportdate = $i;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->role = $p->role_id;
                            $_SESSION['dqu']['clientReport'][$randomtoken]->scope = $p->scope;
                         
                            $out .= '   <li><a class="mono" href="dashboard2/subscriptionreport?id=' . $randomtoken .'" title= "Download File">'.$s->name.'</a></li>'."\n";
                            $firstreport++;
                        }
                    }
                }
                $subInactiveCnt++;
                echo $out;
                $out = null;
            }
        }
        if($firstDatePrevious == true)
        {
            $out .= '</ul>'."\n";
            echo $out;
            $out = null;
        }
        
        
        if ($firstDatePrevious == false)
        {
            echo '<p>No inactive subscriptions.</p>'."\n";
            $noShowPrevious = true;
        }
        
        // Expand/Collasp goes here //
        // ---- Previous Reports --------------------------------- //
        $totalListCnt =0;
        if( empty($subs) )
        {
            //  Dont show the previous reports //
        } else 
        {
            // Loop thorugh the reports count them //
            $totalListCnt =0;
            foreach ($subs as $key => $s) {
                if ($s->expired == "yes")
                {
                    $list = $dqu->get_subs_report_list( $p->client_id, $key ); 
                    $listCnt = count($list);
                    $listCnt = $listCnt - 1;
                    $totalListCnt = $totalListCnt + $listCnt;
                }
            }
            echo '<br />'."\n";
            if( $totalListCnt < 1 )
            {
                //  Dont show the previous reports //
            } else 
            {
                $out = '        <div class="deque-expander" id="previous_reports">' ."\n";
                $out .= '           <div class="deque-expander-summary">Show Previous Inactive Subscription Reports</div>'."\n";
                $out .= '           <div class="deque-expander-content">'."\n";
                
                // ----------------------
                $showPrevious = false;
                
                // Previous Reports - Loop through each subscription //
                foreach ($subs as $key => $s) {
        
                    if ($s->expired == "yes")
                    {
                        $out .= '<h4>' . $s->name . '</h4>' . "\n" . '<ul>'."\n";
            
                        $list = null;
                        $list = $dqu->get_subs_report_list( $p->client_id, $key );
                        
                        if( empty( $list ) )
                        {
                            $out .= '<li>No reports available</li>' ."\n" . '</ul>'."\n";
                        } else
                        {
                            $previousReportCnt = 0;
                            
                            foreach( $list as $i )
                            {
                                //  Don't display the first report, it is the newest. //
                                if( $previousReportCnt > 0 )
                                {
                                    $randomtoken = $dqu->generate_rand_key( 8 );
                                 
                                    // Create session array //
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->name= $s->name;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->cid = $p->client_id;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->sid = $key;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->reportdate = $i;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->role = $p->role_id;
                                    $_SESSION['dqu']['clientReport'][$randomtoken]->scope = $p->scope;
                                 
                                    $out .= '   <li><a class="mono" href="dashboard2/subscriptionreport?id=' . $randomtoken .'" title= "Download File">'.$i.' CSV</a></li>'."\n";
                                    $showPrevious = true;
                                    //$previousReportCnt++;
                                }
                                $previousReportCnt++;
                            }
                        }
                        if( $previousReportCnt == 1 )
                        {
                            $out .= '   <li>No reports available</li>' ."\n" . '</ul>'."\n";
                        }
                        
                    }
                    if( $showPrevious == true )
                    {
                        $out .= '</ul>'."\n";
                    }
                }
                $out .= '           </div>'."\n";
                $out .= '       </div>'."\n";
            }
            // ----------------------
            echo $out;
        }
        echo '          </div>'."\n";
        echo '      </div>'."\n";
        echo '  </div>'."\n";
        echo '</div><br />'."\n";
        $r++;
    }
} else
{
    echo "<p>You do not have permission to view reports.</p>";
}