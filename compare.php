<?php
// How the user_info page displays the progress.  
// deque.Dashboard.class.php - Line 3479 

function get_progress_cache( $uid )
   {
      $dqu = self::$dqu;

      $db = $dqu->get_db();

      $sql = 'SELECT c.`course_id`, p.`name`, c.`cur_progress` FROM `dqu_progress_cache` AS c '.
      'LEFT JOIN `dqu_products` AS p ON p.`root_pg` = c.`course_id` WHERE c.`user_id` = ? '.
      'AND p.`alternate_view` = \'N\' ORDER BY p.`order`';

      if( ($stmt = $db->prepare( $sql )) === false ) { $dqu->last_err = array( $db->errno, $db->error, $sql ); return null; }
      if( ($stmt->bind_param('i',$uid)) === false ) { $dqu->last_err = 'parameter binding failed'; return null; }
      if( ($stmt->execute()) === false ) { $dqu->last_err = array( $stmt->errno, $stmt->error, $sql ); return null; }
      if( ($stmt->bind_result($course_id,$course_name,$prog)) === false ) { $dqu->last_err = 'result binding failed'; return null; }
      $stmt->store_result();

      //----------------
      $ret = array();

      while( $stmt->fetch() )
      {
         $o = new stdClass;

         $o->course_name = $course_name;
         $o->progress = $prog;

         $ret[$course_id] = $o;
      }

      $stmt->free_result();
      //----------------

      return $ret;
   }


// cron.weekly_reports.php line 197

foreach( $users as $uid => $u )
   {
      set_time_limit( 20 * 60 );

      $out .= '"'.$u->full_name.'","'.$u->username.'"';

      $pgs = $dqu->get_viewable_pgs_from_cache( $uid );

      foreach( $courses as $cid => $c )
      {
         $out .= ',';

         if( $pgs === null ) { $out .= 'ERR1'; continue; }
         if( empty( $pgs ) )
         {
            $dqu->db_log('user '.$uid.' has no view access to any page');
            $out .= 'n/a';
            continue;
         }
         if( $pgs[0] === 'error' ) { $out .= 'ERR2'; continue; }

         $c_tree = $dqu->get_credit_tree_from_cache( $c->root_pg );



         if( $c_tree === null ) { $out .= 'ERR3'; continue; }

         // TODO: Refer to the "test:" area in this file to get the algo. If percent is zero, output "-"
         //       If the user has no access, print "n/a".
         //       If there are any errors, just print "ERR".

         if( $pgs[0] === 'sudo' )
         {
            $credit_pgs = array_keys( $c_tree );
         }
         else
         {
            $dqu->filter_tree( $c_tree, $pgs );

            if( empty( $c_tree ) )
            {
               $out .= 'n/a';
               continue;
            }

            $credit_pgs =  $c_tree );
         }

         $credit_quiz_pgs = array_intersect( $credit_pgs, $quiz_pg_ids );
         $credit_info_pgs = array_diff( $credit_pgs, $credit_quiz_pgs );

         /*
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         This does not call the two procs below with a start date. This is OK as long as the user doesn't have an expired course attempt, which
         currently no non-SCORM user does, but this needs to be fixed. You would get the start date of the user's last non-expired attempt and use
         that for these calls. Do you have a proc for that? You must have such a thing in the global script. I would look there before looking in
         the other report-generating code, because the one(s) I just checked don't bother with a start date either.

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         */

         $credit_info_pgs_visited = $dqu->get_visited_pgs( $uid, $credit_info_pgs, 0 );            // FIXME: SKIPPED START

         $credit_quiz_pgs_passed = $dqu->get_quizzes_by_state( $uid, $credit_quiz_pgs, 'pass' );   // FIXME: SKIPPED START

         if( !is_array($credit_info_pgs_visited) || !is_array( $credit_quiz_pgs_passed ) )
         {
            $out .= 'ERR4';
            continue;
         }

         //-----------------------
         $num = count( $credit_info_pgs_visited ) + count( $credit_quiz_pgs_passed );
         $denom = count($credit_info_pgs) + count($credit_quiz_pgs);
         if( $denom == 0 ) { $out .= 'ERR5'; continue; }
         $percent = floor( ($num * 100) / $denom );

         $out .= '"';
         if( $percent == 0 )
         {
            $out .= '-';
         }
         else
         {
            $out .= $percent.'%';
            $out .= " ($num/$denom pgs)";

            /*
            // shows the last date the user visited the course (actually, only credit pages of the course)
            $last_date = '';
            $dqu->get_last_visited_pg( $uid, $credit_pgs, $last_date );
            $out .= "\n[".$last_date.']';
            */

            if( $percent == 100 )
            {
               $last_comp_date = $dqu->get_latest_non_expired_completion_date( $uid, $c->root_pg );

               if( $last_comp_date === false )
               {
                  $out .= "\n".$dqu->TEMPORARY__calc_course_end( $uid, $credit_pgs );
               }
               else
               {
                  $out .= "\n".$last_comp_date;
               }
            }
            else
            {
               $last_comp_date = $dqu->get_latest_non_expired_completion_date( $uid, $c->root_pg );

               if( $last_comp_date !== false )
               {
                  $out .= "\n".'100% on '.$last_comp_date;
               }
            }
         }
         $out .= '"';
         //-----------------------
      }


//
   function get_visited_pgs( $user_id, array $pgs, $cur_pg, $start_time = null )
   /*
   Returns an array of the pages in $pgs that the user has visited. If $cur_pg is in $pgs, then the returned array ALWAYS includes $cur_pg, even
   if the database query doesn't return it.
   If $start_time is given, the query only finds pages that the user has visited SINCE (>=) that datetime.
   $pgs can be empty, in which case an empty array is returned.
   Returns null if there is a database error.
   */
   {
      if( empty( $pgs ) ) return array();

      $list = implode(',', $pgs );

      //----------------
      if( $this->db === null ) { if( !$this->db_connect() ) return null; }
      $db = $this->db;
      //----------------

      //----------------
      if( $start_time === null )
      {
         $sql = 'select distinct `resource` from `logAccess` where `user` = ? and `resource` in ('.$list.')';
         if( ($stmt = $db->prepare( $sql )) === false ) { 
            $this->last_err = array( $db->errno, $db->error, $sql );
            $this->log(' Database error ' . $db->errno . ' ' . $db->error . ' ' . $sql);
            return null; 
        }
         if( ($stmt->bind_param('i', $user_id)) === false ) { 
            $this->last_err = 'parameter binding failed';
            $this->log(' Parameter binding failed. ');
            return null;
        }
      }
      else
      {
         $sql = 'select distinct `resource` from `logAccess` where `user` = ? and `resource` in ('.$list.') and `datetime` >= ?';
         if( ($stmt = $db->prepare( $sql )) === false ) { 
            $this->last_err = array( $db->errno, $db->error, $sql );
            $this->log(' Database error ' . $db->errno . ' ' . $db->error . ' ' . $sql);
            return null; 
        }
         if( ($stmt->bind_param('is', $user_id, $start_time)) === false ) { 
            $this->last_err = 'parameter binding failed';
            $this->log(' Parameter binding failed. ');
            return null;
        }
      }

      if( ($stmt->execute()) === false ) { 
            $this->last_err = array( $stmt->errno, $stmt->error, $sql );
            $this->log(' Database error ' . $stmt->errno . ' ' . $stmt->error . ' ' . $sql);
            return null; 
        }
      if( ($stmt->bind_result($pg)) === false ) {
            $this->last_err = 'result binding failed';
            $this->log(' Result binding failed. ');
            return null; 
        }
      $stmt->store_result();
      //----------------

      //----------------
      $ret = array();

      while( $stmt->fetch() ) $ret[] = $pg;

      $stmt->free_result();
      //----------------

      // Make sure the CURRENT page is in the list of visited pages.
      // (Otherwise, it might not be if this is the user's first visit to the current page.)
      // But only do this if the current page is one of the pages you were asking about in the first place.
      if( !in_array( $cur_pg, $ret ) && in_array( $cur_pg, $pgs ) ) $ret[] = $cur_pg;

      return $ret;
   }


